﻿using System;
using System.Collections.Generic;
using System.Text;
using FortCodeExercises.Exercise1.Helpers;

namespace FortCodeExercises.Exercise1
{
    /// <summary>
    /// Base class that represents a machine.
    /// </summary>
    public abstract class MachineBase
    {
        // Based on the info provided this is the maximum a machine can go
        protected const int AbsoluteMaxSpeed = 90;
        protected const int DefaultMaxSpeed = 70;

        // By default we want our machines to have a default maximum speed
        public virtual bool HasMaxSpeedLimit => true;

        public virtual Application.Colors Color => Application.Colors.White;

        public abstract Application.Colors? TrimColor { get; }

        public virtual int GetMaximumSpeed => DefaultMaxSpeed;

        public abstract string Description { get; }

        public bool IsDark()
        {
            return Color == Application.Colors.Red ||
                   Color == Application.Colors.Green ||
                   Color == Application.Colors.Black ||
                   Color == Application.Colors.Crimson;
        }

        protected MachineBase()
        {
            // todo: maybe pass in some unique id or custom name here?
            // todo: seems a little restcitrted btween the hard-coded colors on a machine
            // todo: you can never do anything with teh colors?  always getting a green tractor?
        }
    }
}

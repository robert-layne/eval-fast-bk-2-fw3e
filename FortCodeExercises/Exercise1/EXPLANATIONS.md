﻿# Robert Layne Explanations

## Notes:
- making a **huge** assumption that we're allowed to modify this class and make breaking changes considering the commit `a355f22b` verbiage made this ambiguous
- at first decided to work out the hard coded names and stuff with `enums` and had that all good and comitted and then did the last refactor
- that `enum` design could work so pls check history on that design
- ultimatley went with an abstract `MachineBase` class with derived classes (e.g. `BullDozer`)
- there's something funky about the speeds where tractors are going faster than cars? also funky naming on the bool going into the max speed function and it was conceptually backwards (that bug got fixed)
- is there a bug with the dark color trim going on a dark base color?  kept that "bug" in but fyi
- still don't really like how the base colors are embedded in teh classes and are never able to be changes... maybe that's a requirement or not. leaving that alone for now
- lots of minor cleanup with switch statements, if/else, string interpolation, using "isHappy = true" instead of just "isHappy"


﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FortCodeExercises.Exercise1.Helpers
{
    /// <summary>
    /// Generic spot for helpers, constants, etc... if this grows too big start splitting out.
    /// </summary>
    public class Application
    {
        // todo: unused, remove me if keeping the baseclass and derived types design
        public enum MachineTypes
        {
            BullDozer = 0,
            Crane = 1,
            Tractor = 2,
            Truck = 3,
            Car = 4
        }

        public enum Colors
        {
            White,
            Blue,
            Red,
            Brown,
            Yellow,
            Green,
            Black,
            Gold,
            Silver,
            Beige,
            BabyBlue,
            Crimson
        }

        /// <summary>
        /// Will take as input any enum and and return that enum as its defined name (works with nullable enum types)
        /// </summary>
        public static string GetEnumName<T>(T enumValue)
        {
            var checkForNullableType = Nullable.GetUnderlyingType(typeof(T)) ?? typeof(T);
            var name = Enum.GetName(checkForNullableType, enumValue);
            return name;
        }

        /// <summary>
        /// Will take as input a name and attempt to find that entry in the enum, will throw error if not found.
        /// </summary>
        public static T GetEnumFromName<T>(string name)
        {
            T @enum = (T)Enum.Parse(typeof(T), name, true);

            return @enum;
        }
    }
}

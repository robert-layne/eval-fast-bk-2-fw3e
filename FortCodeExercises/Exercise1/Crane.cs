﻿using FortCodeExercises.Exercise1.Helpers;

namespace FortCodeExercises.Exercise1
{
    public class Crane: MachineBase
    {
        public override Application.Colors Color => Application.Colors.Blue;

        public override Application.Colors? TrimColor => IsDark() ? Application.Colors.Black : Application.Colors.White;

        public override int GetMaximumSpeed => HasMaxSpeedLimit ? 70 : 75;

        public override string Description => $"{Color} {nameof(Crane)} [{GetMaximumSpeed}].";
    }
}

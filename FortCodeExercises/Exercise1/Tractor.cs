﻿using FortCodeExercises.Exercise1.Helpers;

namespace FortCodeExercises.Exercise1
{
    public class Tractor: MachineBase
    {
        public override Application.Colors Color => Application.Colors.Green;

        public override Application.Colors? TrimColor => IsDark() ? Application.Colors.Gold : (Application.Colors?) null;

        public override int GetMaximumSpeed => HasMaxSpeedLimit ? 60 : 90;

        public override string Description => $"{Color} {nameof(Tractor)} [{GetMaximumSpeed}].";
    }
}

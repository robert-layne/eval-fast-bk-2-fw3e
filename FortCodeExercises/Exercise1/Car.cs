﻿using FortCodeExercises.Exercise1.Helpers;

namespace FortCodeExercises.Exercise1
{
    public class Car : MachineBase
    {
        public override bool HasMaxSpeedLimit => false;

        public override Application.Colors Color => Application.Colors.Brown;

        public override Application.Colors? TrimColor => null;

        public override int GetMaximumSpeed => HasMaxSpeedLimit ? 70 : 90;

        public override string Description => $"{Color} {nameof(Car)} [{GetMaximumSpeed}].";
    }
}

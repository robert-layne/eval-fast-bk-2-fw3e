﻿using FortCodeExercises.Exercise1.Helpers;

namespace FortCodeExercises.Exercise1
{
    public class Truck: MachineBase
    {
        public override bool HasMaxSpeedLimit => false;

        public override Application.Colors Color => Application.Colors.Yellow;

        public override Application.Colors? TrimColor => null;

        public override string Description => $"{Color} {nameof(Truck)} [{GetMaximumSpeed}].";
    }
}

﻿using FortCodeExercises.Exercise1.Helpers;

namespace FortCodeExercises.Exercise1
{
    public class BullDozer: MachineBase
    {
        public override Application.Colors Color => Application.Colors.Red;

        public override Application.Colors? TrimColor => null;

        public override int GetMaximumSpeed => HasMaxSpeedLimit ? DefaultMaxSpeed : 80;

        public override string Description => $"{Color} {nameof(BullDozer)} [{GetMaximumSpeed}].";
    }
}
